SHELL := /bin/bash
.DEFAULT_GOAL := help

GIT_ROOT := $(shell git rev-parse --show-toplevel)

# colors
color1 = $(shell tput setaf 3)
color2 = $(shell tput setaf 5)
reset = $(shell tput sgr0)

# targets
.PHONY: help
help: ## Displays all available targets and their descriptions
	@echo "Available targets are:"
	@echo
	@awk 'BEGIN { FS = ":.*##" } \
        /^[a-zA-Z_-]+:.*?##/ { \
          printf "$(color1)%-25s$(reset) $(color2)%s$(reset)\n", $$1, $$2 \
        }' $(MAKEFILE_LIST) | sort

.PHONY: tests-run
tests-run: ## Run pytest and show print outputs
	@pipenv run pytest -s

.PHONY: tests-coverage
tests-coverage: ## Run pytest and print test coverage
	@pipenv run pytest --cov-report term-missing --cov

.PHONY: code-format
code-format: ## Runs black and isort to format code
	@echo -e "\n$(color1)Format with black:$(reset)"
	pipenv run black $(GIT_ROOT)

	echo -e "\n$(color1)Sort imports with isort:$(reset)"
	pipenv run isort $(GIT_ROOT)

.PHONY: code-lint
code-lint: ## Runs flake8 and mypy as linter
	@echo -e "\n$(color1)Lint with flake8:$(reset)"
	pipenv run flake8 $(GIT_ROOT)

	echo -e "\n$(color1)Lint with mypy:$(reset)"
	pipenv run mypy $(GIT_ROOT)

.PHONY: pre-commits-run
pre-commits-run: ## Run all git pre-commits
	pipenv run pre-commit run --all-files

.PHONY: bootstrap-fedora
bootstrap-fedora: ## Install pipenv and initiates the environment
	@echo -e "\n$(color1)Install pip:$(reset)"
	sudo dnf install -y pip

	echo -e "\n$(color1)Install pipenv:$(reset)"
	sudo dnf install -y pipenv

	echo -e "\n$(color1)Install virtual python environment:$(reset)"
	pipenv install --dev --ignore-pipfile

	echo -e "\n$(color1)Setup pre-commits:$(reset)"
	pipenv run pre-commit install

.PHONY: hooks-update
hooks-update: ## Installs and updates git hooks
	@echo -e "\n$(color1)Install pre-commit hooks:$(reset)"
	pipenv run pre-commit install
	pipenv run pre-commit install --hook-type post-commit

	@echo -e "\n$(color1)Autoupdate pre-commit hooks:$(reset)"
	pipenv run pre-commit autoupdate

.PHONY: publish
publish: ## Publish a new version of doup.
	@echo -e "$(color1)Remove old builds:$(reset)"
	rm -rf dist/

	@echo -e "\n$(color1)Run tests:$(reset)"
	pipenv run pytest

	@echo -e "\n$(color1)Build application:$(reset)"
	pipenv run python -m build

	@echo -e "$\n(color1)Upload to pypi:$(reset)"
	pipenv run twine upload \
		--repository pypi \
		--username __token__ \
		dist/*
