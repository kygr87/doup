import pytest

from doup.crawler import DirectoryCrawler

testData_getImageName = [
    ("docker_dashy_image: lissy93/dashy:2.1.1", "lissy93/dashy:2.1.1"),
    ("FROM nextcloud:24.0.3-fpm", "nextcloud:24.0.3-fpm"),
    ("        sudo: ALL=(ALL) NOPASSWD:ALL", ""),
    ("      - 80:80", ""),
    ("      - '443:443'", ""),
    (
        "checksum: sha512:5444ecaedcadc20e032e622a8a95f40d157c6abc68aeba1c97ef568e1a704c936848b6b05d1ed574f7e7ee53d219a6a3e7d764a5851dd6f353899a36479fa4f4",
        "",
    ),
]


@pytest.mark.parametrize("string, expected", testData_getImageName)
def test_getImageName(string, expected):
    imageName = DirectoryCrawler.getImageName(string)
    assert imageName == expected


testData_containesDockerImageAndMarker = [
    ("docker_influxdb_image: influxdb:1.0.1", "# doup:jetty", True),
    (
        "docker_plantuml_image: plantuml/plantuml-server:jetty-v1.2022.6",
        "# doup:latest:prod",
        True,
    ),
]


@pytest.mark.parametrize(
    "line, previousLine, expected", testData_containesDockerImageAndMarker
)
def test_containsDockerImageAndMarker(line, previousLine, expected):
    result = DirectoryCrawler.containsDockerImageAndMarker(line, previousLine)
    assert result is expected
