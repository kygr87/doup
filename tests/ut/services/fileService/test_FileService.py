import pytest

from doup.services import FileService

testData_getUpdatedData = [
    (
        "tests/ut/services/fileService/testfile1_origin.yml",
        "NEXTCLOUD_IMAGE: nextcloud:24.0.2-fpm",
        "NEXTCLOUD_IMAGE: nextcloud:25.0.2-fpm",
        "tests/ut/services/fileService/testfile1_expected.yml",
    ),
]


@pytest.mark.parametrize(
    "filenameOrigin, oldValue, newValue, filenameExpected", testData_getUpdatedData
)
def test_getUpdatedData(filenameOrigin, oldValue, newValue, filenameExpected):
    result = FileService.getUpdatedData(filenameOrigin, oldValue, newValue)

    file = open(filenameExpected, "rt")
    expected = file.read()

    assert result == expected
