import pytest

from doup.dto import DockerImage

testData_getNextImageName = [
    ("plantuml/plantuml:1.0", "2.0", "plantuml/plantuml:2.0"),
    ("influxdb:1.0", "2.0", "library/influxdb:2.0"),
]


@pytest.mark.parametrize("imageName, nextTag, expected", testData_getNextImageName)
def test_getNextImageName(imageName, nextTag, expected):
    dockerImage = DockerImage.DockerImage(imageName, "", "")
    result = dockerImage.getNextImageName(nextTag)

    assert expected == result
