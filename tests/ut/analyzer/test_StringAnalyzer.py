import pytest

from doup.analyzer import StringAnalyzer


@pytest.fixture
def stringsWithLetters():

    return ["aksjdajsd", "4a2", "123123a", "$a1"]


@pytest.fixture
def stringsWithoutLetters():
    return ["3242", "$123^%^"]


@pytest.fixture
def stringsWithNumbers():
    return ["3242", "$123^%^", "aaa12"]


@pytest.fixture
def stringsWithoutNumbers():
    return ["kjsdad", "$asd^%^"]


def test_hasLetters_1(stringsWithLetters: list):
    for string in stringsWithLetters:
        result = StringAnalyzer.hasLetters(string)
        assert result is True


def test_hasLetters_2(stringsWithoutLetters: list):
    for string in stringsWithoutLetters:
        result = StringAnalyzer.hasLetters(string)
        assert result is False


def test_hasNumbers_withNumbers(stringsWithNumbers: list):
    for string in stringsWithNumbers:
        result = StringAnalyzer.hasNumbers(string)
        assert result is True


def test_hasNumbers_withoutNumbers(stringsWithoutNumbers: list):
    for string in stringsWithoutNumbers:
        result = StringAnalyzer.hasNumbers(string)
        assert result is False


# -----------------------------------------------------------------------------


@pytest.fixture
def testCase_longestStrings():
    return [
        # [[list of string], "longest string"]
        [["test", "test2", "testtest"], "testtest"]
    ]


def test_getLongestString(testCase_longestStrings: list):
    for testCase in testCase_longestStrings:
        strings = testCase[0]

        result = StringAnalyzer.getLongest(strings)
        expected = testCase[1]

        assert result == expected
