import pytest

from doup.analyzer import TagAnalyzer


@pytest.fixture
def testCases_majorVersionUpdates():
    return [
        ["nextcloud:24.0.1-fpm", "nextcloud:25.0.2-fpm", True],
        ["nextcloud:24.0.1-fpm", "nextcloud:24.0.1-fpm", False],
    ]


def test_hasMajorVersionUpdate(testCases_majorVersionUpdates):
    for testCase in testCases_majorVersionUpdates:
        currentVersion = testCase[0]
        nextVersion = testCase[1]
        expected = testCase[2]

        hasMajorVersionUpdate = TagAnalyzer.hasMajorVersionUpdate(
            currentVersion, nextVersion
        )
        assert hasMajorVersionUpdate is expected


# -----------------------------------------------------------------------------


@pytest.fixture
def testCases_getMajorVersionNumber():
    return [
        ["10.1.0", "10"],
    ]


def test_getMajorVersionNumber(testCases_getMajorVersionNumber):
    for testCase in testCases_getMajorVersionNumber:
        version = testCase[0]
        expected = testCase[1]

        result = TagAnalyzer.getMajorVersionNumber(version)
        assert result == expected


# -----------------------------------------------------------------------------


@pytest.fixture
def testCases_getLongestTag():
    return [
        [
            [
                "nextcloud:25.2.0-bullseye",
                "nextcloud:25.2.0",
                "nextcloud:reallyLongVersionWithoutANumber",
            ],
            "nextcloud:25.2.0-bullseye",
        ]
    ]


def test_getLongestTag(testCases_getLongestTag):
    for testCase in testCases_getLongestTag:
        tags = testCase[0]
        expected = testCase[1]

        result = TagAnalyzer.getLongestTag(tags)
        assert result == expected
