import pytest

from doup.analyzer import ImageNameAnalyzer

testData_getRepository = [
    ("mariadb:10.1.0", "library/mariadb"),
    ("nextcloud/nextcloud:25.1.0", "nextcloud/nextcloud"),
]


@pytest.mark.parametrize("dockerimageName, expected", testData_getRepository)
def test_getRepository(dockerimageName, expected):
    result = ImageNameAnalyzer.getRepository(dockerimageName)
    assert result == expected


testData_getTag = [
    ("plantuml/plantuml:1.0", "1.0"),
    (" nextcloud:24.0.3-fpm ", "24.0.3-fpm"),
]


@pytest.mark.parametrize("dockerimageName, expected", testData_getTag)
def test_getTag(dockerimageName, expected):
    result = ImageNameAnalyzer.getTag(dockerimageName)
    assert result == expected
